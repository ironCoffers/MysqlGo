# MysqlGo

## 简介
MysqlGO基于 https://github.com/jmoiron/sqlx 进行二次封装，封装了golang对MySQL数据库的增删改查操作。

## 项目文件简介
- Config.go 

    MysqlGO数据库配置文件，主要管理数据库配置参数和数据库连接操作。
- Model.go 

    MysqlGo数据库操作处理文件，封装了MySQL数据库的增删改查操作处理。

## 安装说明
MysqlGo是golang下对MySQL数据库操作处理进行二次封装，在项目使用过程中，需要使用mysql driver，因为调用时需要引入
```
import (
    _  "https://github.com/go-sql-driver/mysql"
    "http://gitee.com/ironCoffers/MysqlGo"
) 
```

## 使用说明
1.数据库配置
```
var config := &Config{
    Alias : "配置别名",
    DSN : DSNConfig{
        HostName        :"127.0.0.1",
        HostPort        :"3306",	
        DBName		:"dbname",
        UserName	:"用户名",
        Password	:"密码",
        Charset		:"数据库字符集",	
        Prefix		:"",
    },
        MaxOpenConns 	:"设置最大打开的连接数"
	MaxIdleConns 	:"设置闲置的连接数"
	MaxLifetime	:"闲置连接的最大生命周期"	
}
```
2.验证配置

MysqlGo封装Connect()，利用golang SQL自带的ping()函数测试当前配置是否可用。若配置可用，则会将配置保存下来；否则，则报错提示。
```
Connect(config)
```
<blockquote>
建议Connect()函数在服务启动时调用，以保存数据库配置数据，后期直接调用Model即可对数据库进行增删改查操作
</blockquote>

3.数据库操作

数据库操作利用封装的Model进行数据库的增删改查操作
```
//配置Model
var userModel = &Model{
    TableName : "user", //预设关联的表名
}
```
<blockquote>
TableName为预设Model关联的数据库表名，为后面使用userModel时直接关联表名生成对应的SQL语句
</blockquote>

```
//添加记录
///INSERT INTO user(username, password) VALUE('admin', '123456')
var data = User{
	Account : "admin",
	Password : "123456",
}
id, err := userModel.Add(data)
```
<blockquote>
Add()是Model中处理记录Insert操作的函数，函数传入的参数进行分析拆解，然后拼装成Insert SQL语句，并执行。

id为插入记录成功后返回在数据表中记录id

err为执行过程中出现错误的信息
</blockquote>

```
//添加多条记录
///INSERT INTO user(username, password) VALUE('admin', '123456')
///NSERT INTO user(username, password) VALUE('test', '123456')
///INSERT INTO user(username, password) VALUE('abc', '123456')
var datas = []User{ 
    User {
        Account : "admin",
        Password : "123456",
    },
    User {
        Account : "test",
        Password : "123456",
    },
    User {
        Account : "abc",
        Password : "123456",
    },
}
err := userModel.AddAll(datas...)
```
<blockquote>
AddAll()是Model中处理同一张数据表中多条记录Insert操作的函数，函数传入的参数进行分析拆解，然后拼装成Insert SQL语句，逐条执行。函数内部使用了事务处理机制，如果在操作过程中出现某条记录操作失败，则事务进行回滚；否则完成事务操作。
</blockquote>

```
//更新记录
///UPDATE user SET password = "admin" WHERE username = 'admin'
var datas = map[string]interface{}{
		"password" : "admin123",
}
rows, err := userModel.Where("username = ?", "admin").Update(datas)
```
<blockquote>
Update()是Model中处理数据表记录Update处理的函数。函数处理过程主要有两部分：

1.设置Where条件。若未设置Where，在执行Update()时会报错；

2.Update()函数入参。若未填写参数，在执行Update()时报错。

如Update()执行成功后，会返回Update()影响的行数
</blockquote>

```
//查询数据
///SELECT username, password FROM user WHERE state = 1
user := struct{
    Username string
    Password string
} {}
err := userModel.Field("username", "password").Where("state = ?", 1).Find(&user)
```
<blockquote>
Find()是Model处理查询数据的函数，此函数只返回一条记录。利用反射机制，将查询结果传递给Find()函数的参数。函数参数中的字段名称必须与Field的字段值名称，若不相同则会报错
</blockquote>

```
//查询多条数据
user := []struct {
    Username 	string
    Password	string
}{}

err := userModel.Field("username", "password").Where("state = ?", 1).Limit(1, 10).Select(&user)
			
```
<blockquote>
Select()是Model处理查询多条数据的函数，此函数返回多条记录。利用反射机制，将查询结果传递给Select()函数的参数。函数参数中的字段名称必须与Field的字段值名称，若不相同则会报错
</blockquote>