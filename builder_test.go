package mysqlgo

import (
	"testing"
	"fmt"
)
type UserInfo struct {
	Account  string
	Password string
}

func (user *UserInfo) Table() Table{
	return Table{"b_user", ""}
}

func TestFind(t *testing.T){
	t.Run("Find", func(t *testing.T){
		config := &Config {
			Alias		:"default",
			HostName	:"127.0.0.1",
			HostPort	:"3306",	
			DBName		:"bovine",
			UserName	:"root",
			Password	:"",
			Charset		:"",	
			Prefix		:"",
		}
		Connect(config)
		
		user :=  &UserInfo{}
		err := Model(StructModel(user)).Where("status = ?", 0).Find(user)
		fmt.Println(err, user)

	})
}