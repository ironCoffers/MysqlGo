package mysqlgo

import (
	"reflect"
)

type field struct {
	names 	[]string
	values 	[]interface{}
}

func (f *field) parseData(data interface{}) {
	keys, value := f.parseInterface(data)
	f.names, f.values = keys, value
}

func (f *field) parseDatas(datas ...interface{}) ([][]interface{}, error) {
	if len(datas) == 0 {
		return nil, GetError(DataNull)
	}
	data := datas[0]
	var values [][]interface{}
	keys, value := f.parseInterface(data)
	f.names = keys
	values = append(values, value)
	for i := 1 ; i < len(datas); i++ {
		if !reflect.DeepEqual(data, datas[i]) {
			return nil, GetError(TypeError)
		}
		_, value := f.parseInterface(datas[i])
		values = append(values, value)
	}
	return values, nil
}

func (f *field) parseInterface(data interface{}) (keys []string, values []interface{}) {
	getType  := reflect.TypeOf(data)
	getValue := reflect.ValueOf(data)
	for i := 0; i < getType.NumField(); i ++ {
		keys = append(keys, getType.Field(i).Name)
		values = append(values, getValue.Field(i).Interface())
	}
	return
}