package mysqlgo

import (
	"testing"
	"fmt"
	"github.com/stretchr/testify/assert"
)

func TestConnect(t *testing.T) {
	t.Run("Connect Test", func(t *testing.T){
		testCases := []struct {
			in []*Config
			err	error
		} {
			{
				in  : nil,
				err : GetError(ConfigNull),
			},
			{
				in : []*Config{
					 	{
							Alias		: "",
							HostName	: "127.0.0.1",
							HostPort	: "3306",	
							DBName		: "bovine",
							UserName	: "root",
							Password	: "",
							Charset		: "",	
							Prefix		: "",
					},
				},
				err : GetError(DBAliasNull), 
			}, 
			{
				in : []*Config {
					{
						Alias		: "default",
						HostName	: "127.0.0.1",
						HostPort	: "3306",	
						DBName		: "bovine",
						UserName	: "root",
						Password	: "",
						Charset		: "",	
						Prefix		: "",
					},
				},
				err : fmt.Errorf("sql: unknown driver %q (forgotten import?)", driverName),
			},
			{
				in : []*Config {
					{
						Alias		: "default",
						HostName	: "127.0.0.1",
						HostPort	: "3306",	
						DBName		: "bovine",
						UserName	: "root",
						Password	: "",
						Charset		: "",	
						Prefix		: "",	
					},
				},
				err : nil,
			},
			{
				in : []*Config {
					{
						Alias		: "default",
						HostName	: "",
						HostPort	: "3306",	
						DBName		: "bovine",
						UserName	: "root",
						Password	: "",
						Charset		: "",	
						Prefix		: "",	
					},
				},
				err : GetError(HostNameNull),
			},
			{
				in : []*Config {
					{
						Alias		: "default",
						HostName	: "127.0.0.1",
						HostPort	: "",	
						DBName		: "bovine",
						UserName	: "root",
						Password	: "",
						Charset		: "",	
						Prefix		: "",	
					},
				},
				err : GetError(HostPortNull),
			},
			{
				in : []*Config {
					{
						Alias		: "default",
						HostName	: "127.0.0.1",
						HostPort	: "3306",	
						DBName		: "",
						UserName	: "root",
						Password	: "",
						Charset		: "",	
						Prefix		: "",	
					},
				},
				err : GetError(DBNameNull),
			},
			{
				in : []*Config {
					{
						Alias		: "default",
						HostName	: "127.0.0.1",
						HostPort	: "3306",	
						DBName		: "bovine",
						UserName	: "",
						Password	: "",
						Charset		: "",	
						Prefix		: "",	
					},
				},
				err : GetError(UsernameNull),
			},
		}
		for _, testCase := range testCases {
			dbConfigs = make(map[string]*dbConfig, 0)
			err := Connect(testCase.in...)
			if err != nil {
				if !assert.Equal(t, testCase.err, err) {
					t.Errorf("fail case : %v, err : %v", testCase.in, err)
				}
			} else {
				t.Logf("success case : %v", testCase.in)
			}
		}
	})
}

func TestDB(t *testing.T) {
	t.Run("DB Test", func(t *testing.T) {
		testCases := map[string]struct {
			config []*Config
			in string
			err  error
		}{
			"invaild alias" : {
				in : "alias",
				config : []*Config {
					{
						Alias		: "default",
						HostName	: "127.0.0.1",
						HostPort	: "3306",	
						DBName		: "bovine",
						UserName	: "root",
						Password	: "",
						Charset		: "",	
						Prefix		: "",	
					},
				},
			}, 
			"invaild config" : {
				in : "default",
				config : []*Config {
				},
			}, 
		}

		for key, testCase := range testCases {
			dbConfigs = make(map[string]*dbConfig, 0)
			Connect(testCase.config...)
			db := DB(testCase.in)
			if db == nil {
				t.Errorf("[%s]fail case : %v", key, testCase)
			}
		}
	})
}