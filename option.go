package mysqlgo

//Table 数据表
type Table struct {
	Name 	string	//表名
	Alias	string	//别名
}

//Order 结果排序
type Order struct {
	Field 	string 	//排序字段
	Desc	bool	//排序方式: 默认false为asc排序，true为desc排序
}

//Limit 查询和操作的记录数量
type Limit struct {
	Offset		uint		//起始位置，默认为0
	RowCount	uint		//查询数量，默认为10	
}

//Join 表之间关系 
///Join语句，不用加join前缀
///Join类型，默认0
///0为INNER JOIN: 如果表中有至少一个匹配，则返回行，等同于 JOIN
///1为LEFT JOIN: 即使右表中没有匹配，也从左表返回所有的行
///2为RIGHT JOIN: 即使左表中没有匹配，也从右表返回所有的行
///3为FULL JOIN: 只要其中一个表中存在匹配，就返回行
type Join struct {
	Statement 	string	
	Type		JOINTYPE		
}

//JOINTYPE JOIN类型取值
type JOINTYPE uint

const (
	//JOININNER 0为INNER JOIN: 如果表中有至少一个匹配，则返回行，等同于 JOIN
	JOININNER 	JOINTYPE = 0	
	//JOINLEFT 1为LEFT JOIN: 即使右表中没有匹配，也从左表返回所有的行
	JOINLEFT  	JOINTYPE = 1
	//JOINRIGHT 2为RIGHT JOIN: 即使左表中没有匹配，也从右表返回所有的行
	JOINRIGHT 	JOINTYPE = 2
	//JOINFULL 3为FULL JOIN: 只要其中一个表中存在匹配，就返回行
	JOINFULL 	JOINTYPE = 3
)

//Union 合并Select
type Union struct {
	SelectSQL 	[]string
	All			bool 
}

//Data 数据元素
type Data struct {
	Field	string
	Value	interface{}
}