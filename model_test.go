package mysqlgo

import (
	//"fmt"
	"testing"
	//"errors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
)

var config = &Config {
	Alias 		: "default",
	HostName	: "127.0.0.1",
	HostPort	: "3306",	
	DBName		: "bovine",
	UserName	: "root",
	Password	: "",
	Charset		: "utf8",	
}

type User struct {
	Account  	string
	Password	string
	Status		int
}

func TestQueryRow(t *testing.T) {
	t.Run("Find Test", func(t *testing.T){
		testCases := []struct{
			sql 	string
			args 	[]interface{}
			err 	error
		} {
			{
				args: []interface{}{
					"admin",
				},
				sql : "select * from b_user where username = ?",
			},
		}
		config := &Config {
			Alias		:"default",
			HostName	:"127.0.0.1",
			HostPort	:"3306",	
			DBName		:"bovine",
			UserName	:"root",
			Password	:"",
			Charset		:"",	
			Prefix		:"",
		}
		Connect(config)

		for _, testCase := range testCases {
			row, err := QueryRow(testCase.sql, testCase.args...)
			if !assert.Equal(t, err, testCase.err) {
				t.Errorf("fail case: %v, err: %v", testCase, err)
			} else {
				t.Errorf("row: %v", row)
			}
		}
	})
}

func TestQuery(t *testing.T) {
	t.Run("Find Test", func(t *testing.T){
		testCases := []struct{
			sql 	string
			args 	[]interface{}
			err 	error
		} {
			{
				args: []interface{}{
					1,
				},
				sql : "select * from b_user where state = ?",
				err : nil,
			},
		}
		config := &Config {
			Alias		:"default",
			HostName	:"127.0.0.1",
			HostPort	:"3306",	
			DBName		:"bovine",
			UserName	:"root",
			Password	:"",
			Charset		:"",	
			Prefix		:"",
		}
		Connect(config)

		for _, testCase := range testCases {
			row, err := QueryRow(testCase.sql, testCase.args...)
			if !assert.Equal(t, err, testCase.err) {
				t.Errorf("fail case: %v, err: %v", testCase, err)
			} else {
				t.Errorf("row: %v", row)
			}
		}
	})
}

func TestInsert(t *testing.T) {
	t.Run("Test Insert", func(t *testing.T){
		testCases := []struct {
			args 	 []interface{}
			sql  string
			err  error
		} {
			{
				args  : []interface{}{
					"admin1", 
					"123456",
				},
				sql : "insert into b_user(username, password) values(?,?)",
				err : nil,
			},
		}
		config := &Config {
			Alias		:"default",
			HostName	:"127.0.0.1",
			HostPort	:"3306",	
			DBName		:"bovine",
			UserName	:"root",
			Password	:"",
			Charset		:"",	
			Prefix		:"",
		}
		Connect(config)

		for _, testCase := range testCases {
			_, err := Insert(testCase.sql, testCase.args...)
			if err != nil && !assert.Equal(t, err, testCase.err) {
				t.Fatalf("fail case : %v, err : %v", testCase, err)
			}
		}
	})
}

func TestUpdate(t *testing.T) {
	testCases := []struct {
		in 	 []interface{}
		sql  string
		err  error
	} {
		{
			in  : []interface{}{
				"admin1234",
				"admin1", 
			},
			sql : "update b_user set password = ? where username = ?",
			err : nil,
		},
	}
	config := &Config {
		Alias		:"default",
		HostName	:"127.0.0.1",
		HostPort	:"3306",	
		DBName		:"bovine",
		UserName	:"root",
		Password	:"",
		Charset		:"",	
		Prefix		:"",
	}
	Connect(config)

	for _, testCase := range testCases {
		_, err := Update(testCase.sql, testCase.in...)
		if err != nil && !assert.Equal(t, err, testCase.err) {
			t.Fatalf("fail case : %v, err : %v", testCase, err)
		}
	}
}

func TestDelete(t *testing.T) {
	testCases := []struct {
		in 	 []interface{}
		sql  string
		err  error
	} {
		{
			in  : []interface{}{
				"admin1", 
			},
			sql : "delete from b_user where username = ?",
			err : nil,
		},
	}
	config := &Config {
		Alias		:"default",
		HostName	:"127.0.0.1",
		HostPort	:"3306",	
		DBName		:"bovine",
		UserName	:"root",
		Password	:"",
		Charset		:"",	
		Prefix		:"",
	}
	Connect(config)

	for _, testCase := range testCases {
		_, err := Delete(testCase.sql, testCase.in...)
		if err != nil && !assert.Equal(t, err, testCase.err) {
			t.Fatalf("fail case : %v, err : %v", testCase, err)
		}
	}
}