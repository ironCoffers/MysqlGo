package mysqlgo

import "fmt"

//Error 错误信息
type Error struct {
	code	ErrorCode
	errMsg	string
}

//ErrorCode 错误信息编码
type ErrorCode int

const (
	//TypeError 类型错误
	TypeError		ErrorCode = 30001


	//TableNull 表为空
	TableNull		ErrorCode = 40001
	//FieldNull 字段为空
	FieldNull		ErrorCode = 40002
	//WhereNull 条件为空
	WhereNull		ErrorCode = 40003
	//SQLNull SQL为空
	SQLNull			ErrorCode = 40004
	//ModelNull Model为空
	ModelNull		ErrorCode = 40005
	//UsernameNull 用户名为空
	UsernameNull	ErrorCode = 40006
	//HostNameNull hostname为空
	HostNameNull	ErrorCode = 40007
	//DBNameNull 数据库名为空
	DBNameNull		ErrorCode = 40008
	//ConfigNull 数据库配置为空
	ConfigNull		ErrorCode = 40009
	//DBAliasNull 数据库别名为空
	DBAliasNull		ErrorCode = 40010
	//HostPortNull hostport为空
	HostPortNull	ErrorCode = 40011
	//KeyNull key为空
	KeyNull			ErrorCode = 40012
	//DBNull DB为空
	DBNull			ErrorCode = 40013
	//DataNull Data为空
	DataNull		ErrorCode = 40014
	
)

var errorMsg = map[ErrorCode]string {
	TypeError	: "The type of datas isn't same",
	TableNull 	: "The Tables is NULL",
	FieldNull 	: "The Field is NULL of string",
	WhereNull	: "The Where is NULL of string",
	SQLNull		: "The SQL is null of string",
	ModelNull	: "The model argument must pass a pointer",
	UsernameNull: "The UserName is NULL of string",
	HostNameNull: "The HostName is NULL of string",
	DBNameNull	: "The dbname is NULL of string",
	ConfigNull	: "The Config is Null of string",
	HostPortNull: "The HostPort is NULL of string",
	DBAliasNull : "The DBAlias is Null of string",
	KeyNull		: "The Keys is Null of string",
	DBNull		: "The db is null of pointer",
	DataNull	: "The Data is null",
}

//GetError 获取错误信息
func GetError(code ErrorCode) error {
	msg := errorMsg[code]
	return &Error{code, msg}
}

func (e *Error) Error() string {
	return fmt.Sprintf("[%d: %s]", e.code, e.errMsg)
}